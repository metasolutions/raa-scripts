# RAA scripts

Först så måste du ha en EntryStore instans körande som du ska koppla dig till. Riksantikvarieämbetets instans körs på https://raa.entryscape.net/store

## Installera
Se till att du har git, npm och bower installerat. Sen gör följande:

    npm install
    bower install
    cp config.js_example config.js

Glöm inte bort att sätta lösenordet i config.js.

## Köra import scriptet

    cd import
    ./run importMuseer 4 data/Museer-2015.csv

Här får du förståss ändra siffran 4 till det projektid du vill importera till (tekniska termen är context och syns i URL:en när du klickar dig in i ett projekt).

Om du vill rensa  upp kan du köra scriptet `rm`, kommandot tar ett projektid som parameter, dvs:

    ./run rm 4