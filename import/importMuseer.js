define([
  'store-tools/utils/globals',
  'rdfjson/namespaces',
  'config',
  'store/promiseUtil',
  'dojo/node!babyparse',
], (globals, namespaces, config, promiseUtil, babyparse) => {
  let filePath;
  let contextId;

  namespaces.add('org', 'http://www.w3.org/ns/org#');

  const str2Mcode = {
    'Centrala museer': 'http://kulurarvsdata.se/resurser/aukt/museer/centrala_museer',
    'Kommunala museer': 'http://kulurarvsdata.se/resurser/aukt/museer/komunala_museer',
    'Regionala museer': 'http://kulurarvsdata.se/resurser/aukt/museer/regionala_museer',
    'Övriga museer': 'http://kulurarvsdata.se/resurser/aukt/museer/ovriga_museer',
    'Övriga statliga museer': 'http://kulurarvsdata.se/resurser/aukt/museer/ovriga_statliga_museer',
  };

  if (process.argv.length > 4) {
    contextId = process.argv[3];
    filePath = process.argv[4];
  }
  if (contextId == null || filePath == null) {
    console.log('Required parameters: contextid, relativefilepath');
    return;
  }
  const parsed = babyparse.parseFiles(`../import/${filePath}`, {
    header: true,
  });
  const rows = parsed.data;
  console.log(`Nr of rows${rows.length}`);
  console.dir(`First row${rows[0]}`);
  console.log(`Parsed file: ${filePath}`);

  const entrystore = globals.entrystore;
  entrystore.getAuth().login(config.user, config.password).then(() => {
    const context = globals.entrystore.getContextById(contextId);
    // Add entries to context.
    const srows = rows.slice(0, 10);
    srows.forEach((row) => {
      if (row.Museum) {
        console.log(`Organisation: ${row.Museum}`);
        context.newNamedEntry()
          .addL('skos:prefLabel', row.Museum)
          .add('rdf:type', 'org:Site')
          .addL('http://kulturarvsdata.se/resurser/aukt/kommun', row.Kommun)
          .commit()
          .then((siteEntry) => {
            const porg = context.newNamedEntry()
              .addL('skos:prefLabel', row.Museum)
              .add('rdf:type', 'org:Organization')
              .add('org:hasSite', siteEntry.getResourceURI())
              .add('org:classification', str2Mcode[row.Museikategori]);
            if (row['Wikidata URI']) {
              porg.add('owl:sameAs', row['Wikidata URI']);
            }
            porg.commit();
          });
      }
    });
  });
});
