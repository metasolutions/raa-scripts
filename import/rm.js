define([
  'dojo/_base/array',
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'config',
], (array, globals, message, namespaces, config) => {
  namespaces.add('org', 'http://www.w3.org/ns/org#');

  let ctxtid;
  if (process.argv.length > 3) {
    ctxtid = process.argv[3];
  }

  if (ctxtid == null) {
    message('You need to provide context id to import countries');
  } else {
    const es = globals.entrystore;
    es.getAuth().login(config.user, config.password).then(() => {
      const toDelete = [];
      es.newSolrQuery()
              .context(es.getContextById(ctxtid))
              .rdfType(['org:Organization', 'org:Site']).list()
        .forEach((entry) => {
          toDelete.push(entry);
        })
        .then(() => {
          array.forEach(toDelete, (e) => {
            const label = e.getMetadata().findFirstValue('skos:prefLabel');
            console.log(`Deleting ${label}`);
            e.del();
          });
        });
    });
  }
});
