const cmd = process.argv[2];
dojoConfig = { locale: 'en' };// to run validate, which requires locale
const requirejs = require('requirejs');

requirejs.config({
  nodeRequire: require,
  baseUrl: '../libs',
  packages: [
    {
      name: 'raa',
      location: '..',
    },
    {
      name: 'config',
      location: '..',
      main: 'config.js',
    },
    {
      name: 'md5',
      location: 'md5/js',
      main: 'md5.min',
    },
  ],
  map: {
    '*': {
      has: 'dojo/has', // Use dojos has module since it is more clever.
    },
    'store/rest': {
      'dojo/request': 'dojo/request/node', // Force using node
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/node',  // Force using node
    },
  },
  deps: ['store-tools/utils/fix', `raa/${cmd}`],
});
